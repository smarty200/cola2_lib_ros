# Changelog

## [20.10.0] - 26-10-2020

* Added new diagnostic helper class
* Added `setpoint_selector`

## [3.2.0] - 22-10-2019

* Solve bug in `diagnostic_helper.add()` when using `char*`
* Added captain helper to do blocking service calls
* Renamed c++ library to cola2::ros
* Renamed python library to cola2_ros
* Refactor cola 2 lib and splitted into two libraries, one containg ros and another one without ros. This is the one with ros.

## [3.1.0] - 25-02-2019

* First release
